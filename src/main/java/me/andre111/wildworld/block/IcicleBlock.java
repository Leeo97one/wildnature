/*
 * Copyright (c) 2020 André Schweiger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.andre111.wildworld.block;

import java.util.Random;

import me.andre111.wildworld.WildWorld;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.ShapeContext;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import net.minecraft.world.WorldAccess;
import net.minecraft.world.WorldView;

public class IcicleBlock extends Block {
	protected static final VoxelShape SHAPE = Block.createCuboidShape(3.0D, 0.0D, 3.0D, 13.0D, 16.0D, 13.0D);

	public IcicleBlock(Settings block$Settings_1) {
		super(block$Settings_1);
	}

	@Override
	public BlockState getStateForNeighborUpdate(BlockState state, Direction direction, BlockState newState,
			WorldAccess world, BlockPos pos, BlockPos posFrom) {
		if (!state.canPlaceAt(world, pos)) {
			world.getBlockTickScheduler().schedule(pos, this, 1);
		}

		return super.getStateForNeighborUpdate(state, direction, newState, world, pos, posFrom);
	}

	@Override
	public void scheduledTick(BlockState blockState_1, ServerWorld world_1, BlockPos blockPos_1, Random random_1) {
		if (!blockState_1.canPlaceAt(world_1, blockPos_1)) {
			world_1.breakBlock(blockPos_1, true);
		}
	}

	@Override
	public boolean canPlaceAt(BlockState blockState_1, WorldView viewableWorld_1, BlockPos blockPos_1) {
		Block blockUp = viewableWorld_1.getBlockState(blockPos_1.up()).getBlock();

		// can only place when on ice block
		return blockUp == Blocks.ICE || blockUp == Blocks.PACKED_ICE || blockUp == Blocks.BLUE_ICE;
	}

	@Override
	public VoxelShape getOutlineShape(BlockState blockState_1, BlockView blockView_1, BlockPos blockPos_1,
			ShapeContext context_1) {
		return SHAPE;
	}

	@Override
	public void onEntityCollision(BlockState blockState_1, World world_1, BlockPos blockPos_1, Entity entity_1) {
		if (entity_1 instanceof LivingEntity) {
			// slow entities (but not when falling)
			if (entity_1.lastRenderY == entity_1.getY()) {
				entity_1.slowMovement(blockState_1, new Vec3d(0.8, 1, 0.8));
			}

			// damage entities jumping into on spikes
			if (!world_1.isClient && entity_1.lastRenderY != entity_1.getY()) {
				if (entity_1.getY() - entity_1.lastRenderY >= 0.1) {
					entity_1.damage(WildWorld.DAMAGESOURCE_SPIKES, 3);
				}
			}
		}
	}
}
