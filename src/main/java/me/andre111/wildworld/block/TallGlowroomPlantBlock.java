/*
 * Copyright (c) 2020 André Schweiger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.andre111.wildworld.block;

import me.andre111.wildworld.WildWorld;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.block.BlockState;
import net.minecraft.block.TallPlantBlock;
import net.minecraft.block.enums.DoubleBlockHalf;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.WorldView;

public class TallGlowroomPlantBlock extends TallPlantBlock {

	public TallGlowroomPlantBlock(Settings block$Settings_1) {
		super(block$Settings_1);
	}

	@Environment(EnvType.CLIENT)
	public boolean method_22359(BlockState blockState_1) { // forces bright rendering
		return true;
	}

	@Override
	public boolean canReplace(BlockState blockState_1, ItemPlacementContext itemPlacementContext_1) {
		return false;
	}

	@Override
	public boolean canPlaceAt(BlockState blockState_1, WorldView viewableWorld_1, BlockPos blockPos_1) {
		if (blockState_1.get(HALF) == DoubleBlockHalf.LOWER) {
			return WildWorld.GREEN_GLOWSHROOM.canPlaceAt(blockState_1, viewableWorld_1, blockPos_1);
		} else {
			BlockState blockState_2 = viewableWorld_1.getBlockState(blockPos_1.down(1));
			return blockState_2.getBlock() == this && blockState_2.get(HALF) == DoubleBlockHalf.LOWER;
		}
	}
}
