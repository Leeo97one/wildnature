/*
 * Copyright (c) 2020 André Schweiger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.andre111.wildworld.block;

import java.util.Random;

import me.andre111.wildworld.WildWorld;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.MushroomPlantBlock;
import net.minecraft.block.ShapeContext;
import net.minecraft.block.TallPlantBlock;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import net.minecraft.world.WorldView;

public class GlowshroomPlantBlock extends MushroomPlantBlock {
	protected static final VoxelShape SHAPE = Block.createCuboidShape(3.0D, 0.0D, 3.0D, 13.0D, 9.0D, 13.0D);

	private Block[] specialPlantOnBlocks;

	public GlowshroomPlantBlock(Settings block$Settings_1, Block...specialPlantOnBlocks) {
		super(block$Settings_1);

		this.specialPlantOnBlocks = specialPlantOnBlocks;
	}

	@Environment(EnvType.CLIENT)
	public boolean method_22359(BlockState blockState_1) { // forces bright rendering
		return true;
	}

	@Override
	public VoxelShape getOutlineShape(BlockState blockState_1, BlockView blockView_1, BlockPos blockPos_1, ShapeContext context_1) {
		return SHAPE;
	}

	@Override
	public boolean canPlaceAt(BlockState blockState_1, WorldView viewableWorld_1, BlockPos blockPos_1) {
		BlockPos blockPos_2 = blockPos_1.down(1);
		BlockState blockState_2 = viewableWorld_1.getBlockState(blockPos_2);
		Block block_1 = blockState_2.getBlock();
		if (block_1 != Blocks.MYCELIUM && block_1 != Blocks.PODZOL) {
			return viewableWorld_1.getLightLevel(blockPos_1, 0) < 7 && this.canPlantOnTop(blockState_2, viewableWorld_1, blockPos_2);
		} else {
			return true;
		}
	}

	@Override
	protected boolean canPlantOnTop(BlockState blockState_1, BlockView blockView_1, BlockPos blockPos_1) {
		for(Block block : specialPlantOnBlocks) {
			if(blockState_1.getBlock() == block) {
				return true;
			}
		}

		return blockState_1.isFullCube(blockView_1, blockPos_1);
	}

	@Override
	public boolean isFertilizable(BlockView blockView_1, BlockPos blockPos_1, BlockState blockState_1, boolean boolean_1) {
		return getTallPlantBlock() != null;
	}

	@Override
	public boolean canGrow(World world_1, Random random_1, BlockPos blockPos_1, BlockState blockState_1) {
		return getTallPlantBlock() != null && (double)random_1.nextFloat() < 0.1D;
	}

	@Override
	public void grow(ServerWorld world_1, Random random_1, BlockPos blockPos_1, BlockState blockState_1) {
		TallPlantBlock tallPlantBlock_1 = getTallPlantBlock();
		if (tallPlantBlock_1 != null && tallPlantBlock_1.getDefaultState().canPlaceAt(world_1, blockPos_1) && world_1.isAir(blockPos_1.up())) {
			tallPlantBlock_1.placeAt(world_1, blockPos_1, 2);
		}
	}

	private TallPlantBlock getTallPlantBlock() {
		return (TallPlantBlock) (this == WildWorld.GREEN_GLOWSHROOM ? WildWorld.TALL_GREEN_GLOWSHROOM : null);
	}
}
