/*
 * Copyright (c) 2020 André Schweiger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.andre111.wildworld.block;

import java.util.Random;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.VineBlock;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.state.property.BooleanProperty;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.WorldAccess;

//TODO: there are suggestions to add similar features to sugarcane / bamboo / ...
public class ShearedVineBlock extends VineBlock {

	public ShearedVineBlock(Settings block$Settings_1) {
		super(block$Settings_1);
	}

	@Override
	public void scheduledTick(BlockState blockState_1, ServerWorld world_1, BlockPos blockPos_1, Random random_1) {
		// do nothing
	}

	@Override
	public BlockState getStateForNeighborUpdate(BlockState state, Direction direction, BlockState newState,
			WorldAccess world, BlockPos pos, BlockPos posFrom) {
		if (direction == Direction.DOWN) {
			return super.getStateForNeighborUpdate(state, direction, newState, world, pos, posFrom);
		} else {
			return getAttachmentState(state, world, pos);
		}
	}

	private BlockState getAttachmentState(BlockState state, WorldAccess world, BlockPos pos) {
		boolean isAttached = false;
		
		// check up connection
		if ((Boolean)state.get(UP)) {
			if(shouldConnectTo(world, pos, Direction.DOWN)) {
				isAttached = true;
			} else {
				state = state.with(UP, false);
			}
		}

		// check side connections
		for(Direction dir : Direction.Type.HORIZONTAL) {
			BooleanProperty property = getFacingProperty(dir);
			BlockPos attachedPos = pos.offset(dir);
				
			if (state.get(property) && shouldConnectTo(world, attachedPos, dir)) { // connect to solid blocks
				isAttached = true;
			} else { // also connect to vines above
				BlockState blockStateUp = world.getBlockState(pos.up());
				if(blockStateUp.getBlock() == Blocks.VINE && (Boolean)blockStateUp.get(property)) {
					state = state.with(property, true);
					isAttached = true;
				} else {
					state = state.with(property, false);
				}
			}
		}
		
		// not attached
		return isAttached ? state : Blocks.AIR.getDefaultState();
	}

	public BlockState getFromVineBlockState(BlockState vineState) {
		BlockState state = getDefaultState();

		state = state.with(VineBlock.UP, vineState.get(VineBlock.UP));
		state = state.with(VineBlock.NORTH, vineState.get(VineBlock.NORTH));
		state = state.with(VineBlock.EAST, vineState.get(VineBlock.EAST));
		state = state.with(VineBlock.SOUTH, vineState.get(VineBlock.SOUTH));
		state = state.with(VineBlock.WEST, vineState.get(VineBlock.WEST));

		return state;
	}
}
