/*
 * Copyright (c) 2020 André Schweiger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.andre111.wildworld.block;

import java.util.Random;

import me.andre111.wildworld.Config;
import me.andre111.wildworld.WildWorld;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.ShapeContext;
import net.minecraft.block.Waterloggable;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.fluid.FluidState;
import net.minecraft.fluid.Fluids;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.BooleanProperty;
import net.minecraft.state.property.EnumProperty;
import net.minecraft.state.property.Properties;
import net.minecraft.util.StringIdentifiable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import net.minecraft.world.WorldAccess;
import net.minecraft.world.WorldView;

public class SpeleothemBlock extends Block implements Waterloggable {
	public static final EnumProperty<Type> TYPE;
	public static final BooleanProperty WATERLOGGED;
	protected static final VoxelShape SHAPE = Block.createCuboidShape(3.0D, 0.0D, 3.0D, 13.0D, 16.0D, 13.0D);

	private int defaultDropletChance;

	public SpeleothemBlock(int defaultDropletChance, Settings block$Settings_1) {
		super(block$Settings_1);

		this.defaultDropletChance = defaultDropletChance;

		setDefaultState(stateManager.getDefaultState().with(getTypeProperty(), Type.STALAGMITE).with(WATERLOGGED, false));
	}

	public EnumProperty<Type> getTypeProperty() {
		return TYPE;
	}

	@Override
	public FluidState getFluidState(BlockState blockState_1) {
		return blockState_1.get(WATERLOGGED) ? Fluids.WATER.getStill(false) : super.getFluidState(blockState_1);
	}

	@Override
	public BlockState getStateForNeighborUpdate(BlockState state, Direction direction, BlockState newState,
			WorldAccess world, BlockPos pos, BlockPos posFrom) {
		if (!canStayAt(state, world, pos)) {
			world.getBlockTickScheduler().schedule(pos, this, 1);
		}

		return super.getStateForNeighborUpdate(state, direction, newState, world, pos, posFrom);
	}

	@Override
	public void scheduledTick(BlockState blockState_1, ServerWorld world_1, BlockPos blockPos_1, Random random_1) {
		if (blockState_1.get(WATERLOGGED)) {
			world_1.getFluidTickScheduler().schedule(blockPos_1, Fluids.WATER, Fluids.WATER.getTickRate(world_1));
		}

		if (!canStayAt(blockState_1, world_1, blockPos_1)) {
			world_1.breakBlock(blockPos_1, true);
		}
	}
	
	private boolean canStayAt(BlockState blockState_1, WorldView viewableWorld_1, BlockPos blockPos_1) {
		if(canStayAt(blockState_1, viewableWorld_1, blockPos_1, Direction.UP)) {
			return true;
		}
		if(canStayAt(blockState_1, viewableWorld_1, blockPos_1, Direction.DOWN)) {
			return true;
		}
		
		return false;
	}
	
	private boolean canStayAt(BlockState blockState_1, WorldView viewableWorld_1, BlockPos blockPos_1, Direction connectionDir) {
		// test if this can stay when only taking into account the provided direction
		Type type = blockState_1.get(TYPE);
		
		if(connectionDir == Direction.UP) {
			// test for direct attachment to solid block
			if(type.attachesAbove) {
				if(viewableWorld_1.getBlockState(blockPos_1.up()).isOpaqueFullCube(viewableWorld_1, blockPos_1.up())) {
					return true;
				}
			}
			
			// recursively iterate until and endpoint is found
			if(type.connectsAbove) {
				BlockPos abovePos = blockPos_1.up();
				BlockState aboveState = viewableWorld_1.getBlockState(abovePos);
				if(aboveState.getBlock() == this && aboveState.get(TYPE).connectsBelow) {
					return canStayAt(aboveState, viewableWorld_1, abovePos, Direction.UP);
				}
			}
		} else if(connectionDir == Direction.DOWN) {
			// test for direct attachment to solid block
			if(type.attachesBelow) {
				if(viewableWorld_1.getBlockState(blockPos_1.down(1)).isOpaqueFullCube(viewableWorld_1, blockPos_1.down(1))) {
					return true;
				}
			}

			// recursively iterate until and endpoint is found
			if(type.connectsBelow) {
				BlockPos belowPos = blockPos_1.down(1);
				BlockState belowState = viewableWorld_1.getBlockState(belowPos);
				if(belowState.getBlock() == this && belowState.get(TYPE).connectsAbove) {
					return canStayAt(belowState, viewableWorld_1, belowPos, Direction.DOWN);
				}
			}
		}
		
		return false;
	}

	@Override
	public BlockState getPlacementState(ItemPlacementContext itemPlacementContext_1) {
		World world = itemPlacementContext_1.getWorld();
		BlockPos pos = itemPlacementContext_1.getBlockPos();
		BlockState state = world.getBlockState(pos);
		if(canPlaceAt(state, world, pos)) {
			FluidState fluidState = itemPlacementContext_1.getWorld().getFluidState(pos);
			
			// choose between stalagmite or stalagtite
			BlockState placementState = getDefaultState();
			if(world.getBlockState(pos.up()).isOpaqueFullCube(world, pos.up())) {
				placementState = placementState.with(TYPE, Type.STALAGTITE);
			}
			
			// connect to existing blocks
			if(world.getBlockState(pos.up()).getBlock() == this) {
				placementState = placementState.with(TYPE, Type.STALAGTITE_EXTENSION);
				updateStateToConnect(world, world.getBlockState(pos.up()), pos.up(), Direction.DOWN);
			} else if(world.getBlockState(pos.down(1)).getBlock() == this) {
				placementState = placementState.with(TYPE, Type.STALAGMITE_EXTENSION);
				updateStateToConnect(world, world.getBlockState(pos.down(1)), pos.down(1), Direction.UP);
			}
			
			// and waterlog if placed in water
			return placementState.with(WATERLOGGED, fluidState.getFluid() == Fluids.WATER);
		} else {
			return null;
		}
	}
	
	private void updateStateToConnect(World world, BlockState state, BlockPos pos, Direction connectDir) {
		// update state of block to connect to block below of above
		if(connectDir == Direction.DOWN) {
			if(state.get(TYPE) == Type.STALAGTITE) {
				world.setBlockState(pos, state.with(TYPE, Type.STALAGTITE_CONNECTED));
			} else if(state.get(TYPE) == Type.STALAGTITE_EXTENSION) {
				world.setBlockState(pos, state.with(TYPE, Type.EXTENSION));
			}
		} else if(connectDir == Direction.UP) {
			if(state.get(TYPE) == Type.STALAGMITE) {
				world.setBlockState(pos, state.with(TYPE, Type.STALAGMITE_CONNECTED));
			} else if(state.get(TYPE) == Type.STALAGMITE_EXTENSION) {
				world.setBlockState(pos, state.with(TYPE, Type.EXTENSION));
			}
		}
	}

	@Override
	public boolean canPlaceAt(BlockState blockState_1, WorldView viewableWorld_1, BlockPos blockPos_1) {
		BlockPos blockDownPos = blockPos_1.down(1);
		BlockState blockDown = viewableWorld_1.getBlockState(blockDownPos);
		
		BlockPos blockUpPos = blockPos_1.up();
		BlockState blockUp = viewableWorld_1.getBlockState(blockUpPos);

		// can only place when on solid block or other speleothem
		return blockDown.getBlock() == this || blockDown.isOpaqueFullCube(viewableWorld_1, blockDownPos)
				|| blockUp.getBlock() == this || blockUp.isOpaqueFullCube(viewableWorld_1, blockUpPos);
	}

	@Override
	public VoxelShape getOutlineShape(BlockState blockState_1, BlockView blockView_1, BlockPos blockPos_1, ShapeContext context_1) {
		return SHAPE;
	}

	@Override
	public void onEntityCollision(BlockState blockState_1, World world_1, BlockPos blockPos_1, Entity entity_1) {
		if (entity_1 instanceof LivingEntity) {
			// slow entities (but not when falling)
			if (entity_1.lastRenderY == entity_1.getY()) {
				entity_1.slowMovement(blockState_1, new Vec3d(0.8, 1, 0.8));
			}

			// damage entities falling on spikes
			if (Config.getSpikesDamage() > 0) {
				if (!world_1.isClient && entity_1.lastRenderY != entity_1.getY()) {
					if (entity_1.getY() - entity_1.lastRenderY <= -0.1) {
						if (blockState_1.get(TYPE).doesDamage) {
							entity_1.damage(WildWorld.DAMAGESOURCE_SPIKES, 3);
						}
					}
				}
			}
		}
	}

	@Override
	@Environment(EnvType.CLIENT)
	public void randomDisplayTick(BlockState blockState_1, World world_1, BlockPos blockPos_1, Random random_1) {
		if(blockState_1.get(TYPE).doesDrip) {
			// find position of potential water
			BlockPos waterPos = blockPos_1;
			do {
				waterPos = waterPos.up();
			} while(world_1.getBlockState(waterPos).getBlock() == this);
			waterPos = waterPos.up();
			boolean hasWater = world_1.getFluidState(waterPos).getFluid().matchesType(Fluids.WATER);

			// add drip particles
			BlockPos pos = blockPos_1;
			while(world_1.getBlockState(pos).getBlock() == this) {
				if(random_1.nextInt(defaultDropletChance + (hasWater ? 0 : 25)) == 0) {
					float f1 = random_1.nextFloat() * 0.4f;
					float f2 = random_1.nextFloat() * 0.4f;

					double x = pos.getX() + 0.30 + f1;
					double y = pos.getY() + 0.05 + f1 * f2;
					double z = pos.getZ() + 0.30 + f2;

					world_1.addParticle(ParticleTypes.DRIPPING_WATER, x, y, z, 0.0D, 0.0D, 0.0D);
				}
				pos = pos.up();
			}
		}
	}

	@Override
	protected void appendProperties(StateManager.Builder<Block, BlockState> stateFactory$Builder_1) {
		stateFactory$Builder_1.add(TYPE, WATERLOGGED);
	}

	public static enum Type implements StringIdentifiable {
		STALAGTITE("stalagtite", true, false, true, false, false, false), //
		STALAGTITE_CONNECTED("stalagtite_connected", false, false, true, false, false, true), //
		STALAGTITE_EXTENSION("stalagtite_extension", true, false, false, false, true, false), //

		STALAGMITE("stalagmite", false, true, false, true, false, false), //
		STALAGMITE_CONNECTED("stalagmite_connected", false, true, false, true, true, false), //
		STALAGMITE_EXTENSION("stalagmite_extension", false, true, false, false, false, true), //

		EXTENSION("extension", false, false, false, false, true, true), //

		BOTH("both", false, false, true, true, false, false);

		private final String string;
		private final boolean doesDrip;
		private final boolean doesDamage;

		private final boolean attachesAbove;
		private final boolean attachesBelow;
		private final boolean connectsAbove;
		private final boolean connectsBelow;

		private Type(String string, boolean doesDrip, boolean doesDamage, boolean attachesAbove, boolean attachesBelow, boolean connectsAbove, boolean connectsBelow) {
			this.string = string;
			this.doesDrip = doesDrip;
			this.doesDamage = doesDamage;
			
			this.attachesAbove = attachesAbove;
			this.attachesBelow = attachesBelow;
			this.connectsAbove = connectsAbove;
			this.connectsBelow = connectsBelow;
		}

		@Override
		public String asString() {
			return string;
		}
	}

	static {
		TYPE = EnumProperty.of("type", SpeleothemBlock.Type.class);
		WATERLOGGED = Properties.WATERLOGGED;
	}
}
