/*
 * Copyright (c) 2020 André Schweiger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.andre111.wildworld.gen.feature;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.block.BlockState;
import net.minecraft.world.gen.feature.FeatureConfig;

public class ReplacementFeatureConfig implements FeatureConfig {
	public static final Codec<ReplacementFeatureConfig> CODEC = RecordCodecBuilder.create((instance) -> {
		return instance.group(BlockState.CODEC.fieldOf("state").forGetter((replacementFeatureConfig) -> {
			return replacementFeatureConfig.state;
		}), BlockState.CODEC.fieldOf("replacementState").forGetter((replacementFeatureConfig) -> {
			return replacementFeatureConfig.replacementState;
		}), Codec.FLOAT.fieldOf("chance").forGetter((replacementFeatureConfig) -> {
			return replacementFeatureConfig.chance;
		}), Codec.BOOL.fieldOf("requireAir").forGetter((replacementFeatureConfig) -> {
			return replacementFeatureConfig.requireAir;
		})).apply(instance, ReplacementFeatureConfig::new);
	});

	public final BlockState state;
	public final BlockState replacementState;
	public final float chance;
	public final boolean requireAir;

	public ReplacementFeatureConfig(BlockState state, BlockState replacementState, float chance, boolean requireAir) {
		this.state = state;
		this.replacementState = replacementState;
		this.chance = chance;
		this.requireAir = requireAir;
	}
}
