/*
 * Copyright (c) 2020 André Schweiger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.andre111.wildworld.gen.feature;

import java.util.Random;

import com.mojang.serialization.Codec;

import me.andre111.wildworld.block.SpeleothemBlock;
import net.minecraft.block.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.ServerWorldAccess;
import net.minecraft.world.StructureWorldAccess;
import net.minecraft.world.gen.chunk.ChunkGenerator;
import net.minecraft.world.gen.feature.Feature;

public class SpeleothemFeature extends Feature<SpeleothemFeatureConfig> {

	public SpeleothemFeature(Codec<SpeleothemFeatureConfig> codec) {
		super(codec);
	}

	@Override
	public boolean generate(StructureWorldAccess world, ChunkGenerator generator, Random random, BlockPos pos1, SpeleothemFeatureConfig config) {
		BlockPos.Mutable mpos = pos1.mutableCopy();
		// find air
		while(mpos.getY() > 5 && !world.isAir(mpos)) {
			mpos.move(Direction.DOWN);
		}
		if(!world.isAir(mpos)) return false;
		mpos.move(Direction.UP);
		
		if(isValidAttach(world, mpos, config)) {
			mpos.move(Direction.DOWN);
			int space = getEmptyBlocks(world, mpos);
			
			if(space == 1) {
				//generate single space speleothem connected to both sides
				if(isValidAttach(world, mpos.down(1), config)) {
					setBlockState(world, mpos, config.block.with(SpeleothemBlock.TYPE, SpeleothemBlock.Type.BOTH));
					return true;
				}
			} else if(space > 1) {
				BlockPos topPos = mpos;
				BlockPos bottomPos = mpos.down(space - 1);
				
				boolean generatedStalagtite = false;
				boolean generatedStalagmite = false;
				
				if(isValidAttach(world, topPos.up(), config)) {
					space -= generateStalagtite(world, random, topPos, config, space);
					generatedStalagtite = true;
				}
				if(generatedStalagtite && space > 0 && random.nextInt(3) == 0 && isValidAttach(world, bottomPos.down(1), config)) {
					space -= generateStalagmite(world, random, bottomPos, config, space);
					generatedStalagtite = true;
				}
				
				return generatedStalagtite || generatedStalagmite;
			}
		}
		
		return false;
	}
	
	private int generateStalagtite(ServerWorldAccess world, Random random, BlockPos pos, SpeleothemFeatureConfig config, int space) {
		int length = Math.min(random.nextInt(config.maxLength)+1, space);
		BlockPos.Mutable mpos = pos.mutableCopy();
		
		for(int i = 0; i < length; i++) {
			boolean inWater = world.getBlockState(mpos).getBlock()==Blocks.WATER;
			
			if(i == 0) {
				// start
				if(length == 1) {
					setBlockState(world, mpos, config.block.with(SpeleothemBlock.TYPE, SpeleothemBlock.Type.STALAGTITE).with(SpeleothemBlock.WATERLOGGED, inWater));
				} else {
					setBlockState(world, mpos, config.block.with(SpeleothemBlock.TYPE, SpeleothemBlock.Type.STALAGTITE_CONNECTED).with(SpeleothemBlock.WATERLOGGED, inWater));
				}
			} else if(i < length-1) {
				// middle
				setBlockState(world, mpos, config.block.with(SpeleothemBlock.TYPE, SpeleothemBlock.Type.EXTENSION).with(SpeleothemBlock.WATERLOGGED, inWater));
			} else {
				// end
				if(length < space || !isValidAttach(world, mpos.down(1), config)) {
					setBlockState(world, mpos, config.block.with(SpeleothemBlock.TYPE, SpeleothemBlock.Type.STALAGTITE_EXTENSION).with(SpeleothemBlock.WATERLOGGED, inWater));
				} else {
					setBlockState(world, mpos, config.block.with(SpeleothemBlock.TYPE, SpeleothemBlock.Type.STALAGMITE_CONNECTED).with(SpeleothemBlock.WATERLOGGED, inWater));
				}
			}
			
			mpos.move(Direction.DOWN);
		}
		
		return length;
	}
	
	private int generateStalagmite(ServerWorldAccess world, Random random, BlockPos pos, SpeleothemFeatureConfig config, int space) {
		int length = Math.min(random.nextInt(config.maxLength)+1, space);
		BlockPos.Mutable mpos = pos.mutableCopy();
		
		for(int i = 0; i < length; i++) {
			boolean inWater = world.getBlockState(mpos).getBlock()==Blocks.WATER;
			
			if(i == 0) {
				// start
				if(length == 1) {
					setBlockState(world, mpos, config.block.with(SpeleothemBlock.TYPE, SpeleothemBlock.Type.STALAGMITE).with(SpeleothemBlock.WATERLOGGED, inWater));
				} else {
					setBlockState(world, mpos, config.block.with(SpeleothemBlock.TYPE, SpeleothemBlock.Type.STALAGMITE_CONNECTED).with(SpeleothemBlock.WATERLOGGED, inWater));
				}
			} else if(i < length-1) {
				// middle
				setBlockState(world, mpos, config.block.with(SpeleothemBlock.TYPE, SpeleothemBlock.Type.EXTENSION).with(SpeleothemBlock.WATERLOGGED, inWater));
			} else {
				// end
				setBlockState(world, mpos, config.block.with(SpeleothemBlock.TYPE, SpeleothemBlock.Type.STALAGMITE_EXTENSION).with(SpeleothemBlock.WATERLOGGED, inWater));
			}
			
			mpos.move(Direction.UP);
		}
		
		return length;
	}

	private int getEmptyBlocks(ServerWorldAccess world, BlockPos.Mutable mpos) {
		mpos = mpos.mutableCopy();
		
		int count = 0;
		while(mpos.getY() > 0 && (world.isAir(mpos) || world.getBlockState(mpos).getBlock()==Blocks.WATER)) {
			mpos.move(Direction.DOWN);
			count++;
		}
		return count;
	}
	
	private boolean isValidAttach(ServerWorldAccess world, BlockPos pos, SpeleothemFeatureConfig config) {
		return world.getBlockState(pos).getBlock() == config.attachBlock.getBlock();
	}
}
