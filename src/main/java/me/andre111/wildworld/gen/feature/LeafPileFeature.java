/*
 * Copyright (c) 2020 André Schweiger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.andre111.wildworld.gen.feature;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import com.google.common.collect.ImmutableMap;
import com.mojang.serialization.Codec;

import me.andre111.wildworld.WildWorld;
import me.andre111.wildworld.block.LeafPileBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.StructureWorldAccess;
import net.minecraft.world.gen.chunk.ChunkGenerator;
import net.minecraft.world.gen.feature.DefaultFeatureConfig;
import net.minecraft.world.gen.feature.Feature;

public class LeafPileFeature extends Feature<DefaultFeatureConfig> {
	private static final Map<Block, BlockState> LEAF_MAP;
	
	public LeafPileFeature(Codec<DefaultFeatureConfig> codec) {
		super(codec);
	}

	@Override
	public boolean generate(StructureWorldAccess world, ChunkGenerator generator, Random random, BlockPos pos, DefaultFeatureConfig config) {
		if (world.isAir(pos)) {
			BlockPos.Mutable leafBlockPos = pos.mutableCopy();
			while(leafBlockPos.getY() < world.getHeight() && world.isAir(leafBlockPos)) {
				leafBlockPos.move(Direction.UP);
			}

			Block potentialLeafBlock = world.getBlockState(leafBlockPos).getBlock();
			BlockState potatialLeafPileState = LEAF_MAP.get(potentialLeafBlock);
			if(potatialLeafPileState != null && potatialLeafPileState.canPlaceAt(world, pos)) {
				setBlockState(world, pos, potatialLeafPileState);
				return true;
			}
		}
		
		return false;
	}

	static {
		Map<Block, BlockState> leafMap = new HashMap<>();
		for(LeafPileBlock leafPile : WildWorld.LEAF_PILES) {
			leafMap.put(leafPile.getBaseLeafBlock(), leafPile.getDefaultState());
		}
		LEAF_MAP = ImmutableMap.copyOf(leafMap);
	}
}
