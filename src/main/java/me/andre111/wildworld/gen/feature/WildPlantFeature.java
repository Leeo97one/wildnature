/*
 * Copyright (c) 2020 André Schweiger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.andre111.wildworld.gen.feature;

import java.util.Random;

import com.mojang.serialization.Codec;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.state.property.IntProperty;
import net.minecraft.state.property.Properties;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.StructureWorldAccess;
import net.minecraft.world.gen.chunk.ChunkGenerator;
import net.minecraft.world.gen.feature.Feature;

public class WildPlantFeature extends Feature<WildPlantFeatureConfig> {
	private static final IntProperty[] AGE_PROPERTIES = { Properties.AGE_1, Properties.AGE_2, Properties.AGE_3, Properties.AGE_5, Properties.AGE_7, Properties.AGE_15, Properties.AGE_25 };

	public WildPlantFeature(Codec<WildPlantFeatureConfig> codec) {
		super(codec);
	}

	@Override
	public boolean generate(StructureWorldAccess world, ChunkGenerator generator, Random random, BlockPos pos, WildPlantFeatureConfig config) {
		int count = 0;
		BlockPos.Mutable mpos = pos.mutableCopy();

		if(world.getBlockState(mpos).getBlock() == Blocks.GRASS_BLOCK) {
			world.setBlockState(mpos, Blocks.WATER.getDefaultState(), 2);
			
			for(int i = 0; i < 64; ++i) {
				mpos.set(pos);
				mpos.move(random.nextInt(6) - random.nextInt(6), random.nextInt(2) - random.nextInt(2), random.nextInt(6) - random.nextInt(6));
				
				if (world.isAir(mpos) && world.getBlockState(mpos.down(1)).getBlock() == Blocks.GRASS_BLOCK) {
					// generate random age crop
					BlockState state = config.defaultState;
					for(IntProperty ageProperty : AGE_PROPERTIES) {
						if(state.contains(ageProperty)) {
							state = state.with(ageProperty, random.nextInt(ageProperty.getValues().size()));
							break;
						}
					}
					
					// create crop and farmland
					world.setBlockState(mpos, state, 2);
					world.setBlockState(mpos.down(1), Blocks.FARMLAND.getDefaultState(), 2);
					++count;
				}
			}
		}

		return count > 0;
	}
}
