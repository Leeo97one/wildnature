/*
 * Copyright (c) 2020 André Schweiger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.andre111.wildworld.gen.feature;

import java.util.Random;

import com.mojang.serialization.Codec;

import net.minecraft.block.Blocks;
import net.minecraft.block.VineBlock;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.StructureWorldAccess;
import net.minecraft.world.gen.chunk.ChunkGenerator;
import net.minecraft.world.gen.feature.DefaultFeatureConfig;
import net.minecraft.world.gen.feature.Feature;

public class UndergroundVinesFeature extends Feature<DefaultFeatureConfig> {
	private static final Direction[] DIRECTIONS = Direction.values();

	public UndergroundVinesFeature(Codec<DefaultFeatureConfig> codec) {
		super(codec);
	}

	@Override
	public boolean generate(StructureWorldAccess world, ChunkGenerator generator, Random random, BlockPos pos, DefaultFeatureConfig config) {
		BlockPos.Mutable mpos = pos.mutableCopy();

		for (int y = pos.getY(); y > 0; --y) {
			mpos.set(pos);
			mpos.move(random.nextInt(4) - random.nextInt(4), 0, random.nextInt(4) - random.nextInt(4));
			mpos.setY(y);
			if (world.isAir(mpos)) {
				for (Direction dir : DIRECTIONS) {
					if (dir != Direction.DOWN && dir != Direction.UP && VineBlock.shouldConnectTo(world, new BlockPos(mpos.getX(), mpos.getY(), mpos.getZ()).offset(dir), dir)) {
						
						while(world.isAir(mpos) && y > 0) {
							world.setBlockState(mpos, Blocks.VINE.getDefaultState().with(VineBlock.getFacingProperty(dir), true), 2);
							y--;
							mpos.setY(y);
						}
							
						break;
					}
				}
			}
		}

		return true;
	}
}
