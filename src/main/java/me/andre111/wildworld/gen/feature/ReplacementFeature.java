/*
 * Copyright (c) 2020 André Schweiger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.andre111.wildworld.gen.feature;

import java.util.Random;

import com.mojang.serialization.Codec;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.StructureWorldAccess;
import net.minecraft.world.gen.chunk.ChunkGenerator;
import net.minecraft.world.gen.feature.Feature;

public class ReplacementFeature extends Feature<ReplacementFeatureConfig> {

	public ReplacementFeature(Codec<ReplacementFeatureConfig> codec) {
		super(codec);
	}

	@Override
	public boolean generate(StructureWorldAccess world, ChunkGenerator generator, Random random, BlockPos pos, ReplacementFeatureConfig config) {
		BlockPos.Mutable mpos = pos.mutableCopy();

		for(int x=0; x<16; x++) {
			for(int z=0; z<16; z++) {
				for(int y=0; y<generator.getMaxY(); y++) {
					mpos.set(pos);
					mpos.move(x, y, z);
					
					if(world.getBlockState(mpos) == config.state && random.nextFloat() < config.chance) {
						// check for air if required
						boolean set = !config.requireAir;
						if(config.requireAir) {
							for(Direction dir : Direction.values()) {
								if(world.isAir(mpos.offset(dir))) {
									set = true;
									break;
								}
							}
						}
						
						// set block
						if(set) {
							setBlockState(world, mpos, config.replacementState);
						}
					}
				}
			}
		}
		
		return true;
	}

}
