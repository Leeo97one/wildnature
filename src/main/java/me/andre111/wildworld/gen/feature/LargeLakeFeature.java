/*
 * Copyright (c) 2020 André Schweiger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.andre111.wildworld.gen.feature;

import java.util.Random;

import com.mojang.serialization.Codec;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.Material;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkSectionPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.StructureWorldAccess;
import net.minecraft.world.gen.chunk.ChunkGenerator;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.SingleStateFeatureConfig;
import net.minecraft.world.gen.feature.StructureFeature;

public class LargeLakeFeature extends Feature<SingleStateFeatureConfig> {
	private static final BlockState CAVE_AIR;
	private static final int FILL_HEIGHT = 6;
	private static final int SIZE_X = 32;
	private static final int SIZE_Z = 32;
	private static final int SIZE_Y = 16;

	public LargeLakeFeature(Codec<SingleStateFeatureConfig> codec) {
		super(codec);
	}

	@Override
	public boolean generate(StructureWorldAccess world, ChunkGenerator generator, Random random, BlockPos pos, SingleStateFeatureConfig config) {
		BlockPos.Mutable mpos = pos.mutableCopy();
		
		while (mpos.getY() > SIZE_Y && world.isAir(mpos)) {
			mpos.move(Direction.DOWN);
		}

		if (mpos.getY() <= SIZE_Y) {
			return false;
		} else {
			mpos.move(Direction.DOWN, SIZE_Y);
			BlockPos.Mutable source = mpos.mutableCopy();
			
			// avoid villages
			if (world.getStructures(ChunkSectionPos.from(mpos), StructureFeature.VILLAGE).findAny().isPresent()) {
				return false;
			} else {
				// generate random structure by scattering random spheroids
				boolean[] carve = new boolean[32*32*16];
				int count = random.nextInt(8) + 10;

				for (int i = 0; i < count; ++i) {
					double width = random.nextDouble() * 8 + 6;
					double height = random.nextDouble() * 8 + 4;
					double length = random.nextDouble() * 8 + 6;
					double randX = random.nextDouble() * (SIZE_X - width - 2) + 1 + width / 2;
					double randY = random.nextDouble() * (SIZE_Y - height - 4) + 2 + height / 2;
					double randZ = random.nextDouble() * (SIZE_Z - length - 2) + 1 + length / 2;

					for (int x = 1; x < SIZE_X-1; ++x) {
						for (int z = 1; z < SIZE_Z-1; ++z) {
							for (int y = 1; y < SIZE_Y-1; ++y) {
								double normDistX = ((double) x - randX) / (width / 2);
								double normDistY = ((double) y - randY) / (height / 2);
								double normDistZ = ((double) z - randZ) / (length / 2);
								double normDistSQ = normDistX * normDistX + normDistY * normDistY + normDistZ * normDistZ;
								if (normDistSQ < 1.0D) {
									carve[(x * SIZE_Z + z) * SIZE_Y + y] = true;
								}
							}
						}
					}
				}

				// check for valid space
				boolean boolean_2;
				for (int x = 0; x < SIZE_X; ++x) {
					for (int z = 0; z < SIZE_Z; ++z) {
						for (int y = 0; y < SIZE_Y; ++y) {
							// only check blocks we do not want to carve, but are adjacent to carved blocks
							boolean_2 = !carve[(x * SIZE_Z + z) * SIZE_Y + y]
									&& (x < SIZE_X-1 && carve[((x + 1) * SIZE_Z + z) * SIZE_Y + y] || x > 0 && carve[((x - 1) * SIZE_Z + z) * SIZE_Y + y]
									 || z < SIZE_Z-1 && carve[(x * SIZE_Z + (z + 1)) * SIZE_Y + y] || z > 0 && carve[(x * SIZE_Z + (z - 1)) * SIZE_Y + y]
									 || y < SIZE_Y-1 && carve[(x * SIZE_Z + z) * SIZE_Y + (y + 1)] || y > 0 && carve[(x * SIZE_Z + z) * SIZE_Y + (y - 1)]);
							
							if (boolean_2) {
								mpos.set(source);
								mpos.move(x, y, z);
								
								// do not carve air next to a liquid
								Material material = world.getBlockState(mpos).getMaterial();
								if (y >= FILL_HEIGHT && material.isLiquid()) {
									return false;
								}

								// do not carve filling next to non solid block
								if (y < FILL_HEIGHT && !material.isSolid() && world.getBlockState(mpos) != config.state) {
									return false;
								}
							}
						}
					}
				}

				// set blocks
				for (int x = 0; x < SIZE_X; ++x) {
					for (int z = 0; z < SIZE_Z; ++z) {
						for (int y = 0; y < SIZE_Y; ++y) {
							if (carve[(x * SIZE_Z + z) * SIZE_Y + y]) {
								mpos.set(source);
								mpos.move(x, y, z);
								
								world.setBlockState(mpos, y >= FILL_HEIGHT ? CAVE_AIR : config.state, 2);
							}
						}
					}
				}

				return true;
			}
		}
	}

	static {
		CAVE_AIR = Blocks.CAVE_AIR.getDefaultState();
	}
}
