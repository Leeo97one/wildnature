/*
 * Copyright (c) 2020 André Schweiger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.andre111.wildworld.mixedin;

import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import net.minecraft.world.gen.GenerationStep;
import net.minecraft.world.gen.carver.ConfiguredCarver;
import net.minecraft.world.gen.feature.ConfiguredFeature;

public interface ModifiableGenerationSettings {
	public void setCarvers(Map<GenerationStep.Carver, List<Supplier<ConfiguredCarver<?>>>> carvers);
	public void setFeatures(List<List<Supplier<ConfiguredFeature<?, ?>>>> features);
}
