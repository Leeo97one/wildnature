/*
 * Copyright (c) 2020 André Schweiger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.andre111.wildworld;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import me.andre111.wildworld.Config.BiomeConfig;
import me.andre111.wildworld.block.GlowshroomPlantBlock;
import me.andre111.wildworld.block.IcicleBlock;
import me.andre111.wildworld.block.LeafPileBlock;
import me.andre111.wildworld.block.ShearedVineBlock;
import me.andre111.wildworld.block.SpeleothemBlock;
import me.andre111.wildworld.block.TallGlowroomPlantBlock;
import me.andre111.wildworld.block.ThornsBlock;
import me.andre111.wildworld.damage.CustomDamageSource;
import me.andre111.wildworld.gen.carver.LargeCaveCarver;
import me.andre111.wildworld.gen.feature.LargeLakeFeature;
import me.andre111.wildworld.gen.feature.LeafPileFeature;
import me.andre111.wildworld.gen.feature.MarkerNOOPFeature;
import me.andre111.wildworld.gen.feature.PlaceNextToFeature;
import me.andre111.wildworld.gen.feature.PlaceNextToFeatureConfig;
import me.andre111.wildworld.gen.feature.ReplacementFeature;
import me.andre111.wildworld.gen.feature.ReplacementFeatureConfig;
import me.andre111.wildworld.gen.feature.SpeleothemFeature;
import me.andre111.wildworld.gen.feature.SpeleothemFeatureConfig;
import me.andre111.wildworld.gen.feature.SpiderDenFeature;
import me.andre111.wildworld.gen.feature.UndergroundVinesFeature;
import me.andre111.wildworld.gen.feature.WildPlantFeature;
import me.andre111.wildworld.gen.feature.WildPlantFeatureConfig;
import me.andre111.wildworld.mixedin.ModifiableGenerationSettings;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.fabricmc.fabric.api.client.rendering.v1.ColorProviderRegistry;
import net.fabricmc.fabric.api.event.player.UseBlockCallback;
import net.minecraft.block.AbstractPlantStemBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.Material;
import net.minecraft.block.MaterialColor;
import net.minecraft.client.color.block.BlockColorProvider;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.BlockItem;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.Direction;
import net.minecraft.util.registry.DynamicRegistryManager;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.gen.CountConfig;
import net.minecraft.world.gen.GenerationStep;
import net.minecraft.world.gen.ProbabilityConfig;
import net.minecraft.world.gen.carver.ConfiguredCarver;
import net.minecraft.world.gen.decorator.ChanceDecoratorConfig;
import net.minecraft.world.gen.decorator.Decorator;
import net.minecraft.world.gen.decorator.DecoratorConfig;
import net.minecraft.world.gen.decorator.NopeDecoratorConfig;
import net.minecraft.world.gen.decorator.RangeDecoratorConfig;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.DecoratedFeatureConfig;
import net.minecraft.world.gen.feature.DefaultFeatureConfig;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.FeatureConfig;
import net.minecraft.world.gen.feature.RandomPatchFeatureConfig;
import net.minecraft.world.gen.feature.SingleStateFeatureConfig;
import net.minecraft.world.gen.placer.DoublePlantPlacer;
import net.minecraft.world.gen.placer.SimpleBlockPlacer;
import net.minecraft.world.gen.stateprovider.SimpleBlockStateProvider;

public class WildWorld implements ModInitializer, ClientModInitializer {
	public static final String MOD_ID = "wildworld";
	public static final Logger LOGGER = LogManager.getLogger();

	public static final Block GREEN_GLOWSHROOM = new GlowshroomPlantBlock(FabricBlockSettings.of(Material.PLANT).sounds(BlockSoundGroup.GRASS).lightLevel(6).noCollision().ticksRandomly().breakInstantly().drops(new Identifier(MOD_ID, "blocks/green_glowshroom")));
	public static final Block BLUE_GLOWSHROOM = new GlowshroomPlantBlock(FabricBlockSettings.of(Material.PLANT).sounds(BlockSoundGroup.GRASS).lightLevel(6).noCollision().ticksRandomly().breakInstantly().drops(new Identifier(MOD_ID, "blocks/blue_glowshroom")), Blocks.ICE, Blocks.PACKED_ICE, Blocks.BLUE_ICE);
	public static final Block TALL_GREEN_GLOWSHROOM = new TallGlowroomPlantBlock(FabricBlockSettings.of(Material.PLANT).sounds(BlockSoundGroup.GRASS).lightLevel(6).noCollision().breakInstantly().dropsLike(GREEN_GLOWSHROOM));

	public static final SpeleothemBlock STONE_SPELEOTHEM = new SpeleothemBlock(5, FabricBlockSettings.of(Material.STONE, MaterialColor.STONE).sounds(BlockSoundGroup.STONE).strength(1.5F, 6.0F).noCollision().drops(new Identifier(MOD_ID, "blocks/stone_speleothem")));
	public static final SpeleothemBlock SANDSTONE_SPELEOTHEM = new SpeleothemBlock(35, FabricBlockSettings.of(Material.STONE, MaterialColor.SAND).sounds(BlockSoundGroup.STONE).strength(0.8F, 3.0F).noCollision().drops(new Identifier(MOD_ID, "blocks/sandstone_speleothem")));
	public static final IcicleBlock ICICLE = new IcicleBlock(FabricBlockSettings.of(Material.ICE, MaterialColor.ICE).sounds(BlockSoundGroup.GLASS).strength(0.5F, 0.5F).noCollision().drops(new Identifier(MOD_ID, "blocks/icicle")));

	public static final LeafPileBlock OAK_LEAF_PILE = new LeafPileBlock(Blocks.OAK_LEAVES, FabricBlockSettings.of(Material.PLANT, MaterialColor.FOLIAGE).sounds(BlockSoundGroup.GRASS).strength(0.2F, 0.3F).noCollision().drops(new Identifier(MOD_ID, "blocks/oak_leaf_pile")));
	public static final LeafPileBlock SPRUCE_LEAF_PILE = new LeafPileBlock(Blocks.SPRUCE_LEAVES, FabricBlockSettings.of(Material.PLANT, MaterialColor.FOLIAGE).sounds(BlockSoundGroup.GRASS).strength(0.2F, 0.3F).noCollision().drops(new Identifier(MOD_ID, "blocks/spruce_leaf_pile")));
	public static final LeafPileBlock BIRCH_LEAF_PILE = new LeafPileBlock(Blocks.BIRCH_LEAVES, FabricBlockSettings.of(Material.PLANT, MaterialColor.FOLIAGE).sounds(BlockSoundGroup.GRASS).strength(0.2F, 0.3F).noCollision().drops(new Identifier(MOD_ID, "blocks/birch_leaf_pile")));
	public static final LeafPileBlock JUNGLE_LEAF_PILE = new LeafPileBlock(Blocks.JUNGLE_LEAVES, FabricBlockSettings.of(Material.PLANT, MaterialColor.FOLIAGE).sounds(BlockSoundGroup.GRASS).strength(0.2F, 0.3F).noCollision().drops(new Identifier(MOD_ID, "blocks/jungle_leaf_pile")));
	public static final LeafPileBlock ACACIA_LEAF_PILE = new LeafPileBlock(Blocks.ACACIA_LEAVES, FabricBlockSettings.of(Material.PLANT, MaterialColor.FOLIAGE).sounds(BlockSoundGroup.GRASS).strength(0.2F, 0.3F).noCollision().drops(new Identifier(MOD_ID, "blocks/acacia_leaf_pile")));
	public static final LeafPileBlock DARK_OAK_LEAF_PILE = new LeafPileBlock(Blocks.DARK_OAK_LEAVES, FabricBlockSettings.of(Material.PLANT, MaterialColor.FOLIAGE).sounds(BlockSoundGroup.GRASS).strength(0.2F, 0.3F).noCollision().drops(new Identifier(MOD_ID, "blocks/dark_oak_leaf_pile")));
	public static final LeafPileBlock[] LEAF_PILES = {
			OAK_LEAF_PILE, SPRUCE_LEAF_PILE, BIRCH_LEAF_PILE, JUNGLE_LEAF_PILE, ACACIA_LEAF_PILE, DARK_OAK_LEAF_PILE
	};

	public static final ThornsBlock NETHER_THORNS = new ThornsBlock(Blocks.SOUL_SAND, FabricBlockSettings.of(Material.PLANT).sounds(BlockSoundGroup.GRASS).noCollision().strength(1.0F, 1.0f).drops(new Identifier(MOD_ID, "blocks/nether_thorns")));
	public static final ThornsBlock END_THORNS = new ThornsBlock(Blocks.END_STONE, FabricBlockSettings.of(Material.PLANT).sounds(BlockSoundGroup.GRASS).noCollision().strength(1.0F, 1.0f).drops(new Identifier(MOD_ID, "blocks/end_thorns")));

	public static final ShearedVineBlock SHEARED_VINE = new ShearedVineBlock(FabricBlockSettings.of(Material.PLANT).noCollision().strength(0.2F, 0.2f).sounds(BlockSoundGroup.GRASS).dropsLike(Blocks.VINE));

	public static final DamageSource DAMAGESOURCE_SPIKES = (new CustomDamageSource("spikes")).setBypassesArmor();
	public static final DamageSource DAMAGESOURCE_THORNS = (new CustomDamageSource("sthorns")).setBypassesArmor().setUnblockable();

	public static final LargeCaveCarver LARGE_CAVE_CARVER = new LargeCaveCarver(ProbabilityConfig.CODEC, 256);

	public static final MarkerNOOPFeature MARKER_FEATURE = new MarkerNOOPFeature(DefaultFeatureConfig.CODEC);
	
	public static final LargeLakeFeature LARGE_LAKE_FEATURE = new LargeLakeFeature(SingleStateFeatureConfig.CODEC);
	public static final SpeleothemFeature SPELEOTHEM_FEATURE = new SpeleothemFeature(SpeleothemFeatureConfig.CODEC);
	public static final ReplacementFeature REPLACEMENT_FEATURE = new ReplacementFeature(ReplacementFeatureConfig.CODEC);
	public static final UndergroundVinesFeature UNDERGROUND_VINES_FEATURE = new UndergroundVinesFeature(DefaultFeatureConfig.CODEC);
	public static final PlaceNextToFeature PLACE_NEXT_TO_FEATURE = new PlaceNextToFeature(PlaceNextToFeatureConfig.CODEC);
	public static final SpiderDenFeature SPIDER_DEN_FEATURE = new SpiderDenFeature(DefaultFeatureConfig.CODEC);
	public static final LeafPileFeature LEAF_PILE_FEATURE = new LeafPileFeature(DefaultFeatureConfig.CODEC);
	public static final WildPlantFeature WILD_PLANT_FEATURE = new WildPlantFeature(WildPlantFeatureConfig.CODEC);

	@Override
	public void onInitialize() {
		try {
			Config.init(new File("./config/"+MOD_ID+"/"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Register Blocks
		Registry.register(Registry.BLOCK, new Identifier(MOD_ID, "green_glowshroom"), GREEN_GLOWSHROOM);
		Registry.register(Registry.BLOCK, new Identifier(MOD_ID, "blue_glowshroom"), BLUE_GLOWSHROOM);
		Registry.register(Registry.BLOCK, new Identifier(MOD_ID, "tall_green_glowshroom"), TALL_GREEN_GLOWSHROOM);

		Registry.register(Registry.BLOCK, new Identifier(MOD_ID, "stone_speleothem"), STONE_SPELEOTHEM);
		Registry.register(Registry.BLOCK, new Identifier(MOD_ID, "sandstone_speleothem"), SANDSTONE_SPELEOTHEM);
		Registry.register(Registry.BLOCK, new Identifier(MOD_ID, "icicle"), ICICLE);

		Registry.register(Registry.BLOCK, new Identifier(MOD_ID, "oak_leaf_pile"), OAK_LEAF_PILE);
		Registry.register(Registry.BLOCK, new Identifier(MOD_ID, "spruce_leaf_pile"), SPRUCE_LEAF_PILE);
		Registry.register(Registry.BLOCK, new Identifier(MOD_ID, "birch_leaf_pile"), BIRCH_LEAF_PILE);
		Registry.register(Registry.BLOCK, new Identifier(MOD_ID, "jungle_leaf_pile"), JUNGLE_LEAF_PILE);
		Registry.register(Registry.BLOCK, new Identifier(MOD_ID, "acacia_leaf_pile"), ACACIA_LEAF_PILE);
		Registry.register(Registry.BLOCK, new Identifier(MOD_ID, "dark_oak_leaf_pile"), DARK_OAK_LEAF_PILE);

		Registry.register(Registry.BLOCK, new Identifier(MOD_ID, "nether_thorns"), NETHER_THORNS);
		Registry.register(Registry.BLOCK, new Identifier(MOD_ID, "end_thorns"), END_THORNS);

		Registry.register(Registry.BLOCK, new Identifier(MOD_ID, "sheared_vine"), SHEARED_VINE);

		// Register Items
		Registry.register(Registry.ITEM, new Identifier(MOD_ID, "green_glowshroom"), new BlockItem(GREEN_GLOWSHROOM, new Item.Settings().group(ItemGroup.DECORATIONS)));
		Registry.register(Registry.ITEM, new Identifier(MOD_ID, "blue_glowshroom"), new BlockItem(BLUE_GLOWSHROOM, new Item.Settings().group(ItemGroup.DECORATIONS)));

		Registry.register(Registry.ITEM, new Identifier(MOD_ID, "stone_speleothem"), new BlockItem(STONE_SPELEOTHEM, new Item.Settings().group(ItemGroup.DECORATIONS)));
		Registry.register(Registry.ITEM, new Identifier(MOD_ID, "sandstone_speleothem"), new BlockItem(SANDSTONE_SPELEOTHEM, new Item.Settings().group(ItemGroup.DECORATIONS)));
		Registry.register(Registry.ITEM, new Identifier(MOD_ID, "icicle"), new BlockItem(ICICLE, new Item.Settings().group(ItemGroup.DECORATIONS)));

		Registry.register(Registry.ITEM, new Identifier(MOD_ID, "oak_leaf_pile"), new BlockItem(OAK_LEAF_PILE, new Item.Settings().group(ItemGroup.DECORATIONS)));
		Registry.register(Registry.ITEM, new Identifier(MOD_ID, "spruce_leaf_pile"), new BlockItem(SPRUCE_LEAF_PILE, new Item.Settings().group(ItemGroup.DECORATIONS)));
		Registry.register(Registry.ITEM, new Identifier(MOD_ID, "birch_leaf_pile"), new BlockItem(BIRCH_LEAF_PILE, new Item.Settings().group(ItemGroup.DECORATIONS)));
		Registry.register(Registry.ITEM, new Identifier(MOD_ID, "jungle_leaf_pile"), new BlockItem(JUNGLE_LEAF_PILE, new Item.Settings().group(ItemGroup.DECORATIONS)));
		Registry.register(Registry.ITEM, new Identifier(MOD_ID, "acacia_leaf_pile"), new BlockItem(ACACIA_LEAF_PILE, new Item.Settings().group(ItemGroup.DECORATIONS)));
		Registry.register(Registry.ITEM, new Identifier(MOD_ID, "dark_oak_leaf_pile"), new BlockItem(DARK_OAK_LEAF_PILE, new Item.Settings().group(ItemGroup.DECORATIONS)));

		Registry.register(Registry.ITEM, new Identifier(MOD_ID, "nether_thorns"), new BlockItem(NETHER_THORNS, new Item.Settings().group(ItemGroup.DECORATIONS)));
		Registry.register(Registry.ITEM, new Identifier(MOD_ID, "end_thorns"), new BlockItem(END_THORNS, new Item.Settings().group(ItemGroup.DECORATIONS)));

		// Register Decorator and Features
		Registry.register(Registry.CARVER, new Identifier(MOD_ID, "large_cave"), LARGE_CAVE_CARVER);
		
		Registry.register(Registry.FEATURE, new Identifier(MOD_ID, "marker_noop"), MARKER_FEATURE);

		Registry.register(Registry.FEATURE, new Identifier(MOD_ID, "large_lake"), LARGE_LAKE_FEATURE);
		Registry.register(Registry.FEATURE, new Identifier(MOD_ID, "speleothem"), SPELEOTHEM_FEATURE);
		Registry.register(Registry.FEATURE, new Identifier(MOD_ID, "replacement"), REPLACEMENT_FEATURE);
		Registry.register(Registry.FEATURE, new Identifier(MOD_ID, "underground_vines"), UNDERGROUND_VINES_FEATURE);
		Registry.register(Registry.FEATURE, new Identifier(MOD_ID, "place_next_to"), PLACE_NEXT_TO_FEATURE);
		Registry.register(Registry.FEATURE, new Identifier(MOD_ID, "spider_den"), SPIDER_DEN_FEATURE);
		Registry.register(Registry.FEATURE, new Identifier(MOD_ID, "leaf_pile"), LEAF_PILE_FEATURE);
		Registry.register(Registry.FEATURE, new Identifier(MOD_ID, "wild_plant"), WILD_PLANT_FEATURE);

		// add shear callback to vines and similar growing plants to stop growth
		UseBlockCallback.EVENT.register((player, world, hand, blockHitResult) -> {
			if(!world.isClient && !player.isSpectator()) {
				// check for shear useage
				ItemStack item = player.getStackInHand(hand);
				if(item != null && item.getItem() == Items.SHEARS) {
					BlockState state = world.getBlockState(blockHitResult.getBlockPos());
					Block block = state.getBlock();
					boolean sheared = false;
					
					// act on the different blocks
					//TODO: shearing sugar cane, bamboo, ...
					if(block == Blocks.VINE) {
						// vines
						BlockState newState = SHEARED_VINE.getFromVineBlockState(state);
						world.setBlockState(blockHitResult.getBlockPos(), newState);

						// remove vines below
						if(blockHitResult.getBlockPos().getY() > 0) {
							BlockState belowState = world.getBlockState(blockHitResult.getBlockPos().down(1));
							Block belowBlock = belowState.getBlock();
							if(belowBlock == Blocks.VINE || belowBlock == SHEARED_VINE) {
								world.setBlockState(blockHitResult.getBlockPos().down(1), Blocks.AIR.getDefaultState());
							}
						}
						
						sheared = true;
					} else if (block instanceof AbstractPlantStemBlock && state.get(AbstractPlantStemBlock.AGE) < 25) {
						// kelp, weeping vines, twisted vines
						world.setBlockState(blockHitResult.getBlockPos(), state.with(AbstractPlantStemBlock.AGE, 25));

						sheared = true;
					}

					// item damage + sound
					if(sheared) {
						item.setDamage(item.getDamage()+1);
						world.playSound(player, blockHitResult.getBlockPos(), SoundEvents.ENTITY_SHEEP_SHEAR, SoundCategory.BLOCKS, 1f, 1f);

						return ActionResult.SUCCESS;
					}
				}
			}
			return ActionResult.PASS;
		});
	}

	@Override
	public void onInitializeClient() {
		// set render layers
		BlockRenderLayerMap.INSTANCE.putBlock(GREEN_GLOWSHROOM, RenderLayer.getCutout());
		BlockRenderLayerMap.INSTANCE.putBlock(BLUE_GLOWSHROOM, RenderLayer.getCutout());
		BlockRenderLayerMap.INSTANCE.putBlock(TALL_GREEN_GLOWSHROOM, RenderLayer.getCutout());

		BlockRenderLayerMap.INSTANCE.putBlock(STONE_SPELEOTHEM, RenderLayer.getCutout());
		BlockRenderLayerMap.INSTANCE.putBlock(SANDSTONE_SPELEOTHEM, RenderLayer.getCutout());
		BlockRenderLayerMap.INSTANCE.putBlock(ICICLE, RenderLayer.getTranslucent());

		for(LeafPileBlock leafPileBlock : LEAF_PILES) {
			BlockRenderLayerMap.INSTANCE.putBlock(leafPileBlock, RenderLayer.getCutoutMipped());
		}

		BlockRenderLayerMap.INSTANCE.putBlock(NETHER_THORNS, RenderLayer.getCutout());
		BlockRenderLayerMap.INSTANCE.putBlock(END_THORNS, RenderLayer.getCutout());

		BlockRenderLayerMap.INSTANCE.putBlock(SHEARED_VINE, RenderLayer.getCutout());

		// transfer color of blocks
		for(LeafPileBlock leafPile : LEAF_PILES) {
			ColorProviderRegistry.BLOCK.register((block, pos, world, layer) -> {
				BlockColorProvider provider = ColorProviderRegistry.BLOCK.get(leafPile.getBaseLeafBlock());
				return provider == null ? -1 : provider.getColor(block, pos, world, layer);
			}, leafPile);
		}
		ColorProviderRegistry.BLOCK.register((block, pos, world, layer) -> {
			BlockColorProvider provider = ColorProviderRegistry.BLOCK.get(Blocks.VINE);
			return provider == null ? -1 : provider.getColor(block, pos, world, layer);
		}, SHEARED_VINE);

		// transfer color of items
		ColorProviderRegistry.ITEM.register((item, layer) -> {
			Block block = ((BlockItem) item.getItem()).getBlock();
			BlockColorProvider provider = ColorProviderRegistry.BLOCK.get(block);
			return provider.getColor(block.getDefaultState(), null, null, layer);
		}, LEAF_PILES);
	}
	
	public static void addFeatures(DynamicRegistryManager.Impl registryManager) {
		Registry<Biome> biomeRegistry = registryManager.get(Registry.BIOME_KEY);
		biomeRegistry.forEach(biome -> addFeatures(biomeRegistry, biome));
	}

	// method_28614 was configure
	// method_30374 was createDecoratedFeature
	public static void addFeatures(Registry<Biome> registry, Biome biome) {
		if(biome == Biomes.THE_VOID) {
			return;
		}
		LOGGER.debug("Testing "+biome);
		// only modify biomes if it has not already been done
		// TODO: Note: This makes generation fixed after world is created -> config options only apply to new worlds
		if(!biome.getGenerationSettings().getFeatures().isEmpty()) {
			for(Supplier<ConfiguredFeature<?, ?>> fsupList : biome.getGenerationSettings().getFeatures().get(0)) {
				if(fsupList.get().feature == MARKER_FEATURE) {
					LOGGER.debug(" -> features present");
					return;
				}
			}
		}
		addFeature(biome, GenerationStep.Feature.RAW_GENERATION, MARKER_FEATURE.configure(new DefaultFeatureConfig()));
		LOGGER.debug(" -> adding features");

		// get matching biome config
		BiomeConfig bc = Config.getBiomeConfigFromBiome(registry, biome);
		if(bc == null) return;

		// apply configured features
		if(bc.getLargeCaveChance() > 0) {
			addCarver(biome, GenerationStep.Carver.AIR, LARGE_CAVE_CARVER.method_28614(new ProbabilityConfig(bc.getLargeCaveChance())));
		}
		if(bc.getLargeLakeChance() != Integer.MAX_VALUE) {
			addFeature(biome, GenerationStep.Feature.UNDERGROUND_STRUCTURES, LARGE_LAKE_FEATURE.configure(new SingleStateFeatureConfig(Blocks.WATER.getDefaultState())).decorate(Decorator.WATER_LAKE.configure(new ChanceDecoratorConfig(bc.getLargeLakeChance()))));
		}

		if(bc.getReplacementPercentage() > 0 && !bc.getReplacementBlock().isEmpty()) {
			addFeature(biome, GenerationStep.Feature.UNDERGROUND_ORES, REPLACEMENT_FEATURE.configure(new ReplacementFeatureConfig(Blocks.STONE.getDefaultState(), Registry.BLOCK.get(new Identifier(bc.getReplacementBlock())).getDefaultState(), bc.getReplacementPercentage(), bc.isReplacementRequiringAir())).decorate(Decorator.NOPE.configure(new NopeDecoratorConfig())));
		}
		if(bc.getSkullCount() > 0) {
			addFeature(biome, GenerationStep.Feature.UNDERGROUND_ORES, configureCountSquareHeightmapDoubleFeature(PLACE_NEXT_TO_FEATURE.configure(new PlaceNextToFeatureConfig(Blocks.SKELETON_SKULL.getDefaultState(), Blocks.STONE.getDefaultState(), Direction.DOWN)), bc.getSkullCount()));
		}
		if(bc.getCobwebCount() > 0) {
			addFeature(biome, GenerationStep.Feature.UNDERGROUND_ORES, configureCountSquareHeightmapDoubleFeature(PLACE_NEXT_TO_FEATURE.configure(new PlaceNextToFeatureConfig(Blocks.COBWEB.getDefaultState(), Blocks.STONE.getDefaultState(), Direction.UP)), bc.getCobwebCount()));
		}
		if(bc.getStoneSpeleothemCount() > 0 && bc.getStoneSpeleothemMaxLength() > 0) {
			addFeature(biome, GenerationStep.Feature.UNDERGROUND_DECORATION, configureCountSquareHeightmapDoubleFeature(SPELEOTHEM_FEATURE.configure(new SpeleothemFeatureConfig(STONE_SPELEOTHEM.getDefaultState(), Blocks.STONE.getDefaultState(), bc.getStoneSpeleothemMaxLength())), bc.getStoneSpeleothemCount()));
		}
		if(bc.getSandstoneSpeleothemCount() > 0 && bc.getSandstoneSpeleothemMaxLength() > 0) {
			addFeature(biome, GenerationStep.Feature.UNDERGROUND_DECORATION, configureCountSquareHeightmapDoubleFeature(SPELEOTHEM_FEATURE.configure(new SpeleothemFeatureConfig(SANDSTONE_SPELEOTHEM.getDefaultState(), Blocks.SMOOTH_SANDSTONE.getDefaultState(), bc.getSandstoneSpeleothemMaxLength())), bc.getSandstoneSpeleothemCount()));
		}
		if(bc.getIcicleCount() > 0) {
			addFeature(biome, GenerationStep.Feature.UNDERGROUND_DECORATION, configureCountSquareHeightmapDoubleFeature(PLACE_NEXT_TO_FEATURE.configure(new PlaceNextToFeatureConfig(ICICLE.getDefaultState(), Blocks.ICE.getDefaultState(), Direction.UP)), bc.getIcicleCount()));
		}
		if(bc.getSpiderDenChance() > 0) {
			addFeature(biome, GenerationStep.Feature.UNDERGROUND_DECORATION, configureChanceSquareRangeFeature(SPIDER_DEN_FEATURE.configure(FeatureConfig.DEFAULT), bc.getSpiderDenChance(), 0, 256));
		}
		if(bc.getVineCount() > 0) {
			addFeature(biome, GenerationStep.Feature.VEGETAL_DECORATION, configureCountSquareRangeFeature(UNDERGROUND_VINES_FEATURE.configure(FeatureConfig.DEFAULT), bc.getVineCount(), 0, bc.getVineMaxHeight()));
		}
		if(bc.getGreenGlowshroomCount() > 0) {
			addFeature(biome, GenerationStep.Feature.VEGETAL_DECORATION, configureCountSquareRangeFeature(Feature.RANDOM_PATCH.configure((new RandomPatchFeatureConfig.Builder(new SimpleBlockStateProvider(GREEN_GLOWSHROOM.getDefaultState()), new SimpleBlockPlacer())).tries(100).cannotProject().build()), bc.getGreenGlowshroomCount(), 0, 50));
		}
		if(bc.getBlueGlowshroomCount() > 0) {
			addFeature(biome, GenerationStep.Feature.VEGETAL_DECORATION, configureCountSquareRangeFeature(Feature.RANDOM_PATCH.configure((new RandomPatchFeatureConfig.Builder(new SimpleBlockStateProvider(BLUE_GLOWSHROOM.getDefaultState()), new SimpleBlockPlacer())).tries(100).cannotProject().build()), bc.getBlueGlowshroomCount(), 0, 50));
		}
		if(bc.getTallGreenGlowshroomCount() > 0) {
			addFeature(biome, GenerationStep.Feature.VEGETAL_DECORATION, configureCountSquareRangeFeature(Feature.RANDOM_PATCH.configure((new RandomPatchFeatureConfig.Builder(new SimpleBlockStateProvider(TALL_GREEN_GLOWSHROOM.getDefaultState()), new DoublePlantPlacer())).tries(64).cannotProject().build()), bc.getTallGreenGlowshroomCount(), 0, bc.getTallGreenGlowshroomMaxHeight()));
		}
		if(bc.getLeafPileCount() > 0) {
			addFeature(biome, GenerationStep.Feature.VEGETAL_DECORATION, configureCountSquareWorldSurfaceFeature(LEAF_PILE_FEATURE.configure(FeatureConfig.DEFAULT), bc.getLeafPileCount()));
		}
		if(bc.getWildWheatCount() > 0) {
			addFeature(biome, GenerationStep.Feature.VEGETAL_DECORATION, configureCountSquareHeightmapDoubleFeature(WILD_PLANT_FEATURE.configure(new WildPlantFeatureConfig(Blocks.WHEAT.getDefaultState())), bc.getWildWheatCount()));
		}
		if(bc.getWildPotatoeCount() > 0) {
			addFeature(biome, GenerationStep.Feature.VEGETAL_DECORATION, configureCountSquareHeightmapDoubleFeature(WILD_PLANT_FEATURE.configure(new WildPlantFeatureConfig(Blocks.POTATOES.getDefaultState())), bc.getWildPotatoeCount()));
		}
		if(bc.getWildCarrotCount() > 0) {
			addFeature(biome, GenerationStep.Feature.VEGETAL_DECORATION, configureCountSquareHeightmapDoubleFeature(WILD_PLANT_FEATURE.configure(new WildPlantFeatureConfig(Blocks.CARROTS.getDefaultState())), bc.getWildCarrotCount()));
		}
		if(bc.getWildBeetrootCount() > 0) {
			addFeature(biome, GenerationStep.Feature.VEGETAL_DECORATION, configureCountSquareHeightmapDoubleFeature(WILD_PLANT_FEATURE.configure(new WildPlantFeatureConfig(Blocks.BEETROOTS.getDefaultState())), bc.getWildBeetrootCount()));
		}
		if(bc.getNetherThornCount() > 0) {
			addFeature(biome, GenerationStep.Feature.UNDERGROUND_DECORATION, configureCountSquareRangeFeature(Feature.RANDOM_PATCH.configure((new RandomPatchFeatureConfig.Builder(new SimpleBlockStateProvider(NETHER_THORNS.getDefaultState()), new SimpleBlockPlacer())).tries(64).cannotProject().build()), bc.getNetherThornCount(), 0, 128));
		}
		if(bc.getEndThornCount() > 0) {
			addFeature(biome, GenerationStep.Feature.UNDERGROUND_DECORATION, configureCountSquareRangeFeature(Feature.RANDOM_PATCH.configure((new RandomPatchFeatureConfig.Builder(new SimpleBlockStateProvider(END_THORNS.getDefaultState()), new SimpleBlockPlacer())).tries(64).cannotProject().build()), bc.getEndThornCount(), 0, 128));
		}
	}
	
	private static void addCarver(Biome biome, GenerationStep.Carver step, ConfiguredCarver<?> carver) {
		Map<GenerationStep.Carver, List<Supplier<ConfiguredCarver<?>>>> carvers = new HashMap<>();
		
		// convert to modifiable map / lists
		for(GenerationStep.Carver cstep : GenerationStep.Carver.values()) {
			List<Supplier<ConfiguredCarver<?>>> carversList = biome.getGenerationSettings().getCarversForStep(cstep);
			if(!(carversList instanceof ArrayList)) carversList = new ArrayList<>(carversList);
			carvers.put(cstep, carversList);
		}
		
		// insert carver
		carvers.get(step).add(() -> carver);
		
		// override original map (using mixin)
		((ModifiableGenerationSettings) biome.getGenerationSettings()).setCarvers(carvers);
	}
	
	private static void addFeature(Biome biome, GenerationStep.Feature step, ConfiguredFeature<?, ?> feature) {
		List<List<Supplier<ConfiguredFeature<?, ?>>>> features = biome.getGenerationSettings().getFeatures();
		
		// convert to modifiable lists
		if(!(features instanceof ArrayList)) features = new ArrayList<>(features);
		int index = step.ordinal();
		for(int i=0; i<=index; i++) {
			if(features.size() <= i) features.add(new ArrayList<>());
			else if(!(features.get(i) instanceof ArrayList)) features.set(i, new ArrayList<>(features.get(i)));
		}
		
		// insert feature
		features.get(index).add(() -> feature);
		
		// override original list (using mixin)
		((ModifiableGenerationSettings) biome.getGenerationSettings()).setFeatures(features);
	}
	
	private static ConfiguredFeature<?, ?> configureCountSquareHeightmapDoubleFeature(ConfiguredFeature<?, ?> feature, int count) {
		ConfiguredFeature<?, ?> feature1 = Feature.DECORATED.configure(new DecoratedFeatureConfig(() -> feature, Decorator.HEIGHTMAP_SPREAD_DOUBLE.configure(DecoratorConfig.DEFAULT)));
		ConfiguredFeature<?, ?> feature2 = Feature.DECORATED.configure(new DecoratedFeatureConfig(() -> feature1, Decorator.SQUARE.configure(DecoratorConfig.DEFAULT)));
		return feature2.decorate(Decorator.COUNT.configure(new CountConfig(count)));
	}
	private static ConfiguredFeature<?, ?> configureCountSquareRangeFeature(ConfiguredFeature<?, ?> feature, int count, int minimum, int maximum) {
		ConfiguredFeature<?, ?> feature1 = Feature.DECORATED.configure(new DecoratedFeatureConfig(() -> feature, Decorator.RANGE.configure(new RangeDecoratorConfig(minimum, minimum, maximum))));
		ConfiguredFeature<?, ?> feature2 = Feature.DECORATED.configure(new DecoratedFeatureConfig(() -> feature1, Decorator.SQUARE.configure(DecoratorConfig.DEFAULT)));
		return feature2.decorate(Decorator.COUNT.configure(new CountConfig(count)));
	}
	private static ConfiguredFeature<?, ?> configureChanceSquareRangeFeature(ConfiguredFeature<?, ?> feature, int chance, int minimum, int maximum) {
		ConfiguredFeature<?, ?> feature1 = Feature.DECORATED.configure(new DecoratedFeatureConfig(() -> feature, Decorator.RANGE.configure(new RangeDecoratorConfig(minimum, minimum, maximum))));
		ConfiguredFeature<?, ?> feature2 = Feature.DECORATED.configure(new DecoratedFeatureConfig(() -> feature1, Decorator.SQUARE.configure(DecoratorConfig.DEFAULT)));
		return feature2.decorate(Decorator.CHANCE.configure(new ChanceDecoratorConfig(chance)));
	}
	private static ConfiguredFeature<?, ?> configureCountSquareWorldSurfaceFeature(ConfiguredFeature<?, ?> feature, int count) {
		ConfiguredFeature<?, ?> feature1 = Feature.DECORATED.configure(new DecoratedFeatureConfig(() -> feature, Decorator.HEIGHTMAP_WORLD_SURFACE.configure(DecoratorConfig.DEFAULT)));
		ConfiguredFeature<?, ?> feature2 = Feature.DECORATED.configure(new DecoratedFeatureConfig(() -> feature1, Decorator.SQUARE.configure(DecoratorConfig.DEFAULT)));
		return feature2.decorate(Decorator.COUNT.configure(new CountConfig(count)));
	}
}
