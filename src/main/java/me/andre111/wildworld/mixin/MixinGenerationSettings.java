/*
 * Copyright (c) 2020 André Schweiger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.andre111.wildworld.mixin;

import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

import me.andre111.wildworld.mixedin.ModifiableGenerationSettings;
import net.minecraft.world.biome.GenerationSettings;
import net.minecraft.world.gen.GenerationStep;
import net.minecraft.world.gen.GenerationStep.Carver;
import net.minecraft.world.gen.carver.ConfiguredCarver;
import net.minecraft.world.gen.feature.ConfiguredFeature;

@Mixin(GenerationSettings.class)
public class MixinGenerationSettings implements ModifiableGenerationSettings {
	@Shadow
	@Final
	private Map<GenerationStep.Carver, List<Supplier<ConfiguredCarver<?>>>> carvers;
	
	@Shadow
	@Final
	private List<List<Supplier<ConfiguredFeature<?, ?>>>> features;


	@Override
	public void setCarvers(Map<Carver, List<Supplier<ConfiguredCarver<?>>>> carvers) {
		this.carvers = carvers;
	}
	
	@Override
	public void setFeatures(List<List<Supplier<ConfiguredFeature<?, ?>>>> features) {
		this.features = features;
	}
}
