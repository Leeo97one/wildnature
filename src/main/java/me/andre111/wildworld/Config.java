/*
 * Copyright (c) 2020 André Schweiger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.andre111.wildworld;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biome.Category;

public class Config {
	private static final int VERSION = 7;
	private static Config values;

	private int version;
	private float thornsDamage = 1.0f;
	private float spikesDamage = 1.0f;
	private boolean debuggingFullBiomeConfig = false;
	private GenerationSettings generation = new GenerationSettings();
	
	private Config() {
		// add default config
		BiomeGroupConfig bgcDefault = new BiomeGroupConfig(BiomeGroup.DEFAULT);
		bgcDefault.config = BiomeConfig.builder().stoneSpeleothemCount(200).skullCount(5).cobwebCount(80).spiderDenChance(1).leafPileCount(50).build();
		generation.biomeGroups.add(bgcDefault);
		
		BiomeGroupConfig bgcDesert = new BiomeGroupConfig(BiomeGroup.DESERT);
		bgcDesert.config = BiomeConfig.builder().stoneSpeleothemCount(200).skullCount(5).cobwebCount(80).spiderDenChance(1).leafPileCount(50).replacementBlock("minecraft:smooth_sandstone").replacementPercentage(0.75F).sandstoneSpeleothemCount(150).build();
		generation.biomeGroups.add(bgcDesert);
		
		BiomeGroupConfig bgcCold = new BiomeGroupConfig(BiomeGroup.COLD);
		bgcCold.config = BiomeConfig.builder().stoneSpeleothemCount(200).skullCount(5).cobwebCount(80).spiderDenChance(1).leafPileCount(50).replacementBlock("minecraft:ice").replacementPercentage(0.50F).replacementRequiringAir(true).blueGlowshroomCount(48).icicleCount(2000).build();
		generation.biomeGroups.add(bgcCold);
		
		BiomeGroupConfig bgcJungle = new BiomeGroupConfig(BiomeGroup.JUNGLE);
		bgcJungle.config = BiomeConfig.builder().stoneSpeleothemCount(200).skullCount(5).cobwebCount(80).spiderDenChance(1).leafPileCount(50).vineCount(50).greenGlowshroomCount(24).tallGreenGlowshroomCount(6).largeCaveChance(0.3f).build();
		generation.biomeGroups.add(bgcJungle);
		
		BiomeGroupConfig bgcForest = new BiomeGroupConfig(BiomeGroup.FOREST);
		bgcForest.config = BiomeConfig.builder().stoneSpeleothemCount(200).skullCount(5).cobwebCount(80).spiderDenChance(1).leafPileCount(50).wildBeetrootCount(2).build();
		generation.biomeGroups.add(bgcForest);
		
		BiomeGroupConfig bgcPlains = new BiomeGroupConfig(BiomeGroup.PLAINS);
		bgcPlains.config = BiomeConfig.builder().stoneSpeleothemCount(200).skullCount(5).cobwebCount(80).spiderDenChance(1).leafPileCount(50).wildWheatCount(2).wildPotatoeCount(1).wildCarrotCount(1).build();
		generation.biomeGroups.add(bgcPlains);
		
		BiomeGroupConfig bgcMountains = new BiomeGroupConfig(BiomeGroup.MOUNTAINS);
		bgcMountains.biomes.add("minecraft:badlands_plateau");
		bgcMountains.biomes.add("minecraft:modified_badlands_plateau");
		bgcMountains.biomes.add("minecraft:wooded_badlands_plateau");
		bgcMountains.biomes.add("minecraft:modified_wooded_badlands_plateau");
		bgcMountains.config = BiomeConfig.builder().stoneSpeleothemCount(250).skullCount(5).cobwebCount(80).spiderDenChance(1).leafPileCount(50).largeCaveChance(0.175F).build();
		generation.biomeGroups.add(bgcMountains);
		
		BiomeGroupConfig bgcHumid = new BiomeGroupConfig(BiomeGroup.HUMID);
		bgcHumid.config = BiomeConfig.builder().stoneSpeleothemCount(200).skullCount(5).cobwebCount(80).spiderDenChance(1).leafPileCount(50).greenGlowshroomCount(12).tallGreenGlowshroomCount(2).build();
		generation.biomeGroups.add(bgcHumid);
		
		BiomeGroupConfig bgcNether = new BiomeGroupConfig(BiomeGroup.NETHER);
		bgcNether.config = BiomeConfig.builder().netherThornCount(16).build();
		generation.biomeGroups.add(bgcNether);
		
		BiomeGroupConfig bgcEnd = new BiomeGroupConfig(BiomeGroup.END);
		bgcEnd.config = BiomeConfig.builder().endThornCount(1).build();
		generation.biomeGroups.add(bgcEnd);
	}

	public static void init(File configDir) throws IOException {
		values = new Config();
		values.version = VERSION;
		
		// loading or creating config file
		if(!configDir.exists()) {
			configDir.mkdirs();
		}
		boolean writeConfig = false;
		File configFile = new File(configDir, "config.json");
		Gson gson = new Gson();
		if(configFile.exists()) {
			try(Reader reader = new FileReader(configFile)) {
				values = gson.fromJson(reader, Config.class);
			}
			
			// create a backup and revert to default config if config version is outdated
			if(values.version < VERSION) {
				File configFileBackup = new File(configDir, "config_old.json");
				Files.copy(configFile.toPath(), configFileBackup.toPath(), StandardCopyOption.REPLACE_EXISTING);
				System.out.println("WARNING: Config file for WildWorld was outdated:");
				System.out.println("Moved backup to config_old.json and reverted to default config!");
				values = new Config();
				values.version = VERSION;
				writeConfig = true;
			}
		} else {
			writeConfig = true;
		}
		
		if(writeConfig) {
			try(JsonWriter writer = new JsonWriter(new FileWriter(configFile))) {
				writer.setIndent("\t");
				gson.toJson(values, Config.class, writer);
			}
		}
	}
	
	public static float getThornsDamage() {
		return values.thornsDamage;
	}

	public static float getSpikesDamage() {
		return values.spikesDamage;
	}

	public static boolean isDebuggingFullBiomeConfig() {
		return values.debuggingFullBiomeConfig;
	}
	
	public static BiomeConfig getBiomeConfigFromBiome(Registry<Biome> registry, Biome biome) {
		BiomeConfig bc = Config.getBiomeConfigFromGroup(BiomeGroup.DEFAULT);
		if(biome.getCategory() == Category.NETHER) {
			bc = Config.getBiomeConfigFromGroup(BiomeGroup.NETHER);
		} else if(biome.getCategory() == Category.THEEND) {
			bc = Config.getBiomeConfigFromGroup(BiomeGroup.END);
		} else if(biome.getCategory() == Category.DESERT) {
			bc = Config.getBiomeConfigFromGroup(BiomeGroup.DESERT);
		} else if(biome.getTemperature() < 0.2F || biome.getCategory() == Category.ICY) {
			bc = Config.getBiomeConfigFromGroup(BiomeGroup.COLD);
		} else if(biome.getCategory() == Category.JUNGLE) {
			bc = Config.getBiomeConfigFromGroup(BiomeGroup.JUNGLE);
		} else if(biome.getCategory() == Category.FOREST) {
			bc = Config.getBiomeConfigFromGroup(BiomeGroup.FOREST);
		} else if(biome.getCategory() == Category.PLAINS) {
			bc = Config.getBiomeConfigFromGroup(BiomeGroup.PLAINS);
		} else if(biome.getCategory() == Category.EXTREME_HILLS) {
			bc = Config.getBiomeConfigFromGroup(BiomeGroup.MOUNTAINS);
		} else if(biome.hasHighHumidity()) {
			bc = Config.getBiomeConfigFromGroup(BiomeGroup.HUMID);
		}
		
		// find specific override
		Identifier id = registry.getId(biome);
		for(BiomeGroupConfig override : values.generation.biomeGroups) {
			for(String biomeID : override.getBiomes()) {
				if(id.equals(Identifier.tryParse(biomeID))) {
					return override.getConfig();
				}
			}
		}
		return bc;
	}
	
	private static BiomeConfig getBiomeConfigFromGroup(BiomeGroup group) {
		for(BiomeGroupConfig override : values.generation.biomeGroups) {
			if(override.group == group) {
				return override.config;
			}
		}
		return null;
	}
	
	private static enum BiomeGroup {
		NONE,
		
		DEFAULT,
		DESERT,
		COLD,
		JUNGLE,
		FOREST,
		PLAINS,
		MOUNTAINS,
		HUMID,
		NETHER,
		END
	}
	
	public static class GenerationSettings {
		private List<BiomeGroupConfig> biomeGroups = new ArrayList<>();
	}

	public static @Data @Builder class BiomeConfig {
		private @Builder.Default final int stoneSpeleothemCount = 0;
		private @Builder.Default final int stoneSpeleothemMaxLength = 4;
		private @Builder.Default final int sandstoneSpeleothemCount = 0;
		private @Builder.Default final int sandstoneSpeleothemMaxLength = 4;

		private @Builder.Default final String replacementBlock = "";
		private @Builder.Default final float replacementPercentage = 0;
		private @Builder.Default final boolean replacementRequiringAir = false;

		private @Builder.Default final int icicleCount = 0;

		private @Builder.Default final int skullCount = 0;
		private @Builder.Default final int cobwebCount = 0;

		private @Builder.Default final int vineCount = 0;
		private @Builder.Default final int vineMaxHeight = 64;

		private @Builder.Default final int greenGlowshroomCount = 0;
		private @Builder.Default final int blueGlowshroomCount = 0;
		private @Builder.Default final int tallGreenGlowshroomCount = 0;
		private @Builder.Default final int tallGreenGlowshroomMaxHeight = 48;
		
		private @Builder.Default final int spiderDenChance = 0;
		
		private @Builder.Default final int leafPileCount = 0;
		
		private @Builder.Default final int wildWheatCount = 0;
		private @Builder.Default final int wildPotatoeCount = 0;
		private @Builder.Default final int wildCarrotCount = 0;
		private @Builder.Default final int wildBeetrootCount = 0;
		
		private @Builder.Default final int netherThornCount = 0;
		private @Builder.Default final int endThornCount = 0;
		
		private @Builder.Default final float largeCaveChance = 0;
		private @Builder.Default final int largeLakeChance = Integer.MAX_VALUE;
	}
	
	public static class BiomeGroupConfig {
		private @Getter BiomeGroup group = BiomeGroup.NONE;
		private @Getter List<String> biomes = new ArrayList<>();
		private @Getter BiomeConfig config;
		
		private BiomeGroupConfig(BiomeGroup group) {
			this.group = group;
		}
	}
}
